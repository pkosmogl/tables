# MC cross section tables

Two-column files with phase space and corresponding cross section in mb 
For instance, the total cross section in the Pythia 8 flat sample is around 2 mb.
For comparison, the total MB cross section at CMS is 100 mb.

## How to get a cross section

### TWiki

High-order calculations for many standard model cross setions can be found
[on the TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/StandardModelCrossSectionsat13TeV).
There are dedicated pages for top and Higgs processes. **Check these first!**

### Cross section Database (xsdb)

[XSDB](https://cms-gen-dev.cern.ch/xsdb/) is a database maintained by the GEN
and PPD groups. Every sample should have at least one cross section there,
extracted at production time with `GenXSecAnalyzer` (see below for a
discussion). Sometimes there can be more than one number, for instance if a
sample was produced twice with different conditions. Values can also be added
manually, but unfortunately there is usually less information than on the
TWiki.

For Py16, we took the value from Qiguo, as instructed by Paolo Gunnellini. For others, not always clear, or not always existent.

### Computing it yourself (GenXSecAnalyzer)

`GenXSecAnalyzer` is a CMSSW module that extracts cross section information
stored by the Monte Carlo generator at generation time. Since it comes from the
generator, this number suffers from the same limitations as the generator
itself. In particular, cross sections calculated at leading order may suffer
from very large scale uncertainties and be subject to large correction factors.
For complicated processes such as top or Higgs production, it is best to rely
on externally computed numbers when available.

The DAS ntupliser module is configured to run GenXSecAnalyzer by default. The
cross sections can be found by reading the job log.

See also the
[instructions](https://twiki.cern.ch/twiki/bin/viewauth/CMS/HowToGenXSecAnalyzer)

### MCM

[McM](https://cms-pdmv.cern.ch/mcm/) is primarily used to produce samples.
There is a cross section field, but updating it is a manual process and errors
are possible. Don't trust this information.

**AVOID IT AS MUCH AS POSSIBLE**
